require 'sinatra'
require 'haml'
require 'redis_orm'
require 'json'
require 'sinatra/content_for'
require 'sinatra/formkeeper'

set :protection, except: :ip_spoofing

configure :production do
	 #Appfog Settings
     services = JSON.parse(ENV['VCAP_SERVICES'])
     redis_key = services.keys.select { |svc| svc =~ /redis/i }.first
     uri = services[redis_key].first['credentials']
	 $redis = Redis.new(:host => uri['hostname'], :port => uri['port'], :password => uri['password'])
	 
	 #Heroku Settings
	 #uri = URI.parse(ENV["REDISTOGO_URL"])
	 #$redis = Redis.new(:host => uri.host, :port => uri.port, :password => uri.password)
end

configure :development do
  $redis = Redis.new(:host => "localhost", :port => "6379")
end

class Auction < RedisOrm::Base 	
	property :email, string
	property :name, string
	property :google_map_url, string
	property :description, string
	property :bid_guid, string
	property :auction_guid, string
	has_many :bids

	index :bid_guid
	index :auction_guid
	index :email

	def high_bid
		high_bid = 0.00
		bids.each do |bid|
			high_bid = bid.value.to_f if bid.value.to_f > high_bid
		end
		return high_bid
	end

end

class Bid < RedisOrm::Base 	
	property :name, string 
	property :email, string 
	property :value, integer
	belongs_to :auction
end

form_messages File.expand_path(File.join(File.dirname(__FILE__), 'form_error_messages.yaml'))

get '/' do
  haml :auction
end

not_found do 
	haml :notFound
end

post '/generateauction' do
 form do 
 	filters :strip
 	field :name, :present => true, :length => 4..100
 	field :email, :present => true, :length => 5..100
 	field :google_map_url, :present => true, :uri => [:http, :https], :bytesize => 10..255
    field :description, :present => true
 end
 if form.failed?
 	haml :auction
 else
 	bid_guid = SecureRandom.urlsafe_base64
 	auction_guid = SecureRandom.urlsafe_base64
 	@auction_details = Auction.create(:name => form[:name], :description => form[:description], :email => form[:email], :google_map_url => form[:google_map_url], :bid_guid => bid_guid, :auction_guid => auction_guid)
 	haml :auctiongenerated
 end
end

get '/bid/:bid_page_guid' do
  @auction_details = Auction.find_by_bid_guid params[:bid_page_guid]
	@auction_high_bid = @auction_details.high_bid
  haml :bid	
end

get '/admin/:auction_page_guid' do
  @auction_details = Auction.find_by_auction_guid params[:auction_page_guid]
  haml :admin	
end

post '/bid/:bid_page_guid' do
  @auction_details = Auction.find_by_bid_guid params[:bid_page_guid]
  form do 
  	filters :strip
  	field :name, :present => true, :length => 4..100
 	field :email, :present => true, :length => 5..100
 	field :value, :present => true
  end
  if form.failed? 
  	haml :bid
  else
	@bid_details = Bid.create(:name => form[:name], :email => form[:email], :value => form[:value], :auction_guid => @auction_details.auction_guid)
	@auction_details.bids << @bid_details
	haml :bidsubmitted	
  end
end



