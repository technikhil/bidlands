Bidlands
========

This is a simple proof of concept that explores ways in which we can make selling land via a bidding process easier. 

There are essentially 3 parts to this - 

* Setting up an auction(tender).
* Submitting bids.
* Viewing and managing the tender.

### Setting up an auction 
This will be a simple form that accepts an email address, a description and a Google Map URL. 
On submission, the link to a bidding page will be auto-generated and provided to the owner of the auction so that he can communicate it to the potential bidders for the land.

### Submitting bids 
On recieving the link to bid in an auction the bidder simply go to the bid page where he is shown the map of the land, and the previous bid along with a form to submit his bid. 

### Viewing and managing the tender
When the tender is set up the auction owner is given another link that they can go to see the status of the auction, examine all the bids and close the auction if needed. This link will be emailed to the auction owner.

### Underlying technologies used 
Have a look at the Gemfile.lock for a list of the libraries used in this application.
In addition we are using JQuery and Twitter Bootstrap